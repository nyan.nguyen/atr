import internals from '../utils/httpClient';

export const getAll = (options) => internals.get('/category',[],{...options});
export const create = (payload,options) => internals.post('/category/create',payload,{...options});
export const get = (payload,options) => internals.get('/category/get/'+payload._id,[],{...options});
export const update = (payload,options) => internals.post('/category/update',payload,{...options});
export const remove = (payload,options) => internals.post('/category/delete',payload,{...options});