import internals from '../utils/httpClient';

export const getAll = (options) => internals.get('/investment',{...options});
export const get = (payload,options) => internals.get('/investment/get/'+payload._id,[],{...options});
export const getCompletedProjects = (options) => internals.get('/investment/get-completed',{...options});
export const getProcessingProjects = (options) => internals.get('/investment/get-processing',{...options});