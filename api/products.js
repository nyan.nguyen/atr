import internals from '../utils/httpClient';

export const getAll = (options) => internals.get('/product',[],{...options});
export const create = (payload,options) => internals.post('/product/create',payload,{...options});
export const get = (payload,options) => internals.get('/product/get/'+payload._id,[],{...options});
export const update = (payload,options) => internals.post('/product/update',payload,{...options});
export const remove = (payload,options) => internals.post('/product/delete',payload,{...options});