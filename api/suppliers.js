import internals from '../utils/httpClient';

export const getAll = (options) => internals.get('/supplier',[],{...options});
export const create = (payload,options) => internals.post('/supplier/create',payload,{...options});
export const get = (payload,options) => internals.get('/supplier/get/'+payload._id,[],{...options});
export const update = (payload,options) => internals.post('/supplier/update',payload,{...options});
export const remove = (payload,options) => internals.post('/supplier/delete',payload,{...options});