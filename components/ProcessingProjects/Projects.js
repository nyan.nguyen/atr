import React from 'react';
import Link from 'next/link';
// import { withTranslation } from 'react-i18next';
import {useTranslation} from 'next-i18next';
import { Image, Typography } from 'antd';

const Projects = ({props}) => {
  const {products} = props;
  const {t} = useTranslation('common');
  return (
    <>
      <section className="blog-area ptb-100">
        <div className="container">
          <div className="row">
            {products.map(product => {
              return(<div className="col-lg-4 col-md-6">
              <div className="single-blog-post text-center">
                <img className="mt-4 cover"src={product.cover}/>

                <div className="post-content">
                  <h3>
                    <Link href={`/product/${product._id}`}>
                      <a>{product?.name?.find(n => n.locale === props._nextI18Next.initialLocale).value}</a>
                    </Link>
                  </h3>
                  <div className="row">
                    <Typography.Text className="short-description-preview">{product?.short_description?.find(n => n.locale === props._nextI18Next.initialLocale)?.value}</Typography.Text>
                  </div>
                  <Link href={`/product/${product._id}`}>
                    <a className="default-btn">{t('Products.14')} <span></span></a>
                  </Link>
                </div>
              </div>
            </div>
            )
            })}
          </div>
        </div>
      </section>
    </>
  );

}



export default Projects;