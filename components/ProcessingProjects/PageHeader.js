import React from 'react';
import Link from 'next/link';
// import { withTranslation } from 'react-i18next';
import {useTranslation} from 'next-i18next';

const PageHeader = () => {
  const {t} = useTranslation('common');
  return (
    <>
      <div className="page-title-area item-bg1">
        <div className="d-table">
          <div className="d-table-cell">
            <div className="container">
              <div className="page-title-content">
                <h2>{t('investment.processing_projects')}</h2>
                <ul>
                  <li>
                    <Link href="/">
                      <a>{t('Home.1')}</a>
                    </Link>
                  </li>
                  <li>{t('investment.processing_projects')}</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );

}



export default PageHeader;