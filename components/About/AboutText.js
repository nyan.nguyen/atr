import React from 'react';
// import { withTranslation } from 'react-i18next';
import {useTranslation} from 'next-i18next';
import {serverSideTranslations} from 'next-i18next/serverSideTranslations';

const AboutText = () => {
  const {t} = useTranslation('common');
  return (
    <>
      <section className="about-area ptb-100">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-12">
              <div className="about-text" dangerouslySetInnerHTML={{
                  __html: t('About Us.body')
                }}>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );

}



export default AboutText;