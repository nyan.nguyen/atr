import React from 'react';
import Link from 'next/link';
// import { withTranslation } from 'react-i18next';
import {useTranslation} from 'next-i18next';
import {serverSideTranslations} from 'next-i18next/serverSideTranslations';

const Footer = () => {
  const {t} = useTranslation('common');
  return (
    <>
      <footer className="footer-area">
        <div className="container px-0">
          {/* <div className="subscribe-area">
            
          </div> */}

          <div className="row">
            <div className="col-12">
              <div className="single-footer-widget">
                <div className="logo">
                  <Link href="/">
                    <a>
                      <img width={70} style={{maxHeight:"none"}} src="/images/white-logo.png" alt="image" className="white-logo"/>
                      <img width={55} style={{maxHeight:"none"}} src="/images/logoTR2.png" alt="image" className="logoTR2"/>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12">
              <div className="single-footer-widget">
                <div className="row">
                  <div className="col-12">
                    <div className="subscribe-content mb-4">
                      <h3 className="text-white">{t('Footer.1')}</h3>
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="subscribe-form px-0">
                      <form className="newsletter-form px-0">
                        <input type="email" className="input-newsletter" placeholder={t('Footer.11')} name="EMAIL"/>
                        <button type="submit">
                          <i className="flaticon-right-chevron"></i>
                        </button>
                      </form>
                    </div>
                  </div>
                  
                  <div className="single-footer-widget text-center">
                    <ul className="social">
                      <li>
                        <Link href="https://www.facebook.com/T-R-Company-Limited-102245858338955">
                          <a target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" data-name="Ebene 1" viewBox="0 0 1024 1024">
                              <path fill="#1877f2" d="M1024,512C1024,229.23016,794.76978,0,512,0S0,229.23016,0,512c0,255.554,187.231,467.37012,432,505.77777V660H302V512H432V399.2C432,270.87982,508.43854,200,625.38922,200,681.40765,200,740,210,740,210V336H675.43713C611.83508,336,592,375.46667,592,415.95728V512H734L711.3,660H592v357.77777C836.769,979.37012,1024,767.554,1024,512Z"/>
                              <path fill="#fff" d="M711.3,660,734,512H592V415.95728C592,375.46667,611.83508,336,675.43713,336H740V210s-58.59235-10-114.61078-10C508.43854,200,432,270.87982,432,399.2V512H302V660H432v357.77777a517.39619,517.39619,0,0,0,160,0V660Z"/>
                            </svg>
                          </a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 112.197 112.197" viewBox="0 0 112.197 112.197">
                              <circle cx="56.099" cy="56.098" r="56.098" fill="#55acee"/>
                              <path fill="#f1f2f2" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417    c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409    c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742    c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17    c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239    c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188    c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734    C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                            </svg>
                          </a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" fill="none" viewBox="0 0 48 48">
                              <path fill="#0077B5" d="M0 24C0 10.7452 10.7452 0 24 0C37.2548 0 48 10.7452 48 24C48 37.2548 37.2548 48 24 48C10.7452 48 0 37.2548 0 24Z"/>
                              <path fill="#fff" fill-rule="evenodd" d="M17.3188 14.8227C17.3188 16.3918 16.1377 17.6473 14.2412 17.6473H14.2064C12.3805 17.6473 11.2 16.3918 11.2 14.8227C11.2 13.2204 12.4164 12 14.277 12C16.1377 12 17.2835 13.2204 17.3188 14.8227ZM16.9605 19.8778V36.2196H11.5216V19.8778H16.9605ZM36.5752 36.2196L36.5754 26.8497C36.5754 21.8303 33.8922 19.4941 30.3131 19.4941C27.4254 19.4941 26.1325 21.0802 25.4107 22.1929V19.8783H19.9711C20.0428 21.4117 19.9711 36.22 19.9711 36.22H25.4107V27.0934C25.4107 26.605 25.446 26.1178 25.5898 25.7681C25.9829 24.7924 26.8779 23.7822 28.3805 23.7822C30.3494 23.7822 31.1365 25.2807 31.1365 27.4767V36.2196H36.5752Z" clip-rule="evenodd"/>
                            </svg>
                          </a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>

                {/* <ul className="social">
                  <li>
                    <Link href="https://www.facebook.com/T-R-Company-Limited-102245858338955">
                      <a target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" data-name="Ebene 1" viewBox="0 0 1024 1024">
                          <path fill="#1877f2" d="M1024,512C1024,229.23016,794.76978,0,512,0S0,229.23016,0,512c0,255.554,187.231,467.37012,432,505.77777V660H302V512H432V399.2C432,270.87982,508.43854,200,625.38922,200,681.40765,200,740,210,740,210V336H675.43713C611.83508,336,592,375.46667,592,415.95728V512H734L711.3,660H592v357.77777C836.769,979.37012,1024,767.554,1024,512Z"/>
                          <path fill="#fff" d="M711.3,660,734,512H592V415.95728C592,375.46667,611.83508,336,675.43713,336H740V210s-58.59235-10-114.61078-10C508.43854,200,432,270.87982,432,399.2V512H302V660H432v357.77777a517.39619,517.39619,0,0,0,160,0V660Z"/>
                        </svg>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="#">
                      <a target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 112.197 112.197" viewBox="0 0 112.197 112.197">
                          <circle cx="56.099" cy="56.098" r="56.098" fill="#55acee"/>
                          <path fill="#f1f2f2" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417    c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409    c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742    c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17    c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239    c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188    c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734    C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                        </svg>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="#">
                      <a target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" fill="none" viewBox="0 0 48 48">
                          <path fill="#0077B5" d="M0 24C0 10.7452 10.7452 0 24 0C37.2548 0 48 10.7452 48 24C48 37.2548 37.2548 48 24 48C10.7452 48 0 37.2548 0 24Z"/>
                          <path fill="#fff" fill-rule="evenodd" d="M17.3188 14.8227C17.3188 16.3918 16.1377 17.6473 14.2412 17.6473H14.2064C12.3805 17.6473 11.2 16.3918 11.2 14.8227C11.2 13.2204 12.4164 12 14.277 12C16.1377 12 17.2835 13.2204 17.3188 14.8227ZM16.9605 19.8778V36.2196H11.5216V19.8778H16.9605ZM36.5752 36.2196L36.5754 26.8497C36.5754 21.8303 33.8922 19.4941 30.3131 19.4941C27.4254 19.4941 26.1325 21.0802 25.4107 22.1929V19.8783H19.9711C20.0428 21.4117 19.9711 36.22 19.9711 36.22H25.4107V27.0934C25.4107 26.605 25.446 26.1178 25.5898 25.7681C25.9829 24.7924 26.8779 23.7822 28.3805 23.7822C30.3494 23.7822 31.1365 25.2807 31.1365 27.4767V36.2196H36.5752Z" clip-rule="evenodd"/>
                        </svg>
                      </a>
                    </Link>
                  </li>
                </ul>
               */}
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12">
              <div className="single-footer-widget">
                <h3>{t('Footer.4')}</h3>
                <ul className="footer-contact-info">
                  <li><span>{t('Footer.5')}</span>{t('Footer.7')}</li>
                  <li><span>Email:</span> <a href="#">atrgroup@atr.com.vn</a></li>
                  <li><span>{t('Footer.9')}</span> <a href="#">+(84) 933 278 434</a></li>
                  <li><a href="https://goo.gl/maps/AC3KcEwgdmQBK6EMA" target="_blank">{t('Footer.10')}</a></li>
                </ul>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12">
              <div className="single-footer-widget">
                <h3>{t('Footer.SendMessage')}</h3>
                <div className="contact-form">
                    <form id="contactForm">
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <input type="text" name="name" id="name" className="form-control" required placeholder="Name" />
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <input type="email" name="email" id="email" className="form-control" required placeholder="Email" />
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <input type="text" name="msg_subject" id="msg_subject" className="form-control" required placeholder="Subject" />
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <textarea name="message" className="form-control" id="message" cols="30" rows="6" required placeholder="Your Message"></textarea>
                                </div>
                            </div>

                            <div className="col-12 text-center">
                                <button type="submit" className="default-btn">
                                {t('Contact Us.7')}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>

          <div className="copyright-area">
            <div className="row align-items-center">
              <div className="col-lg-6 col-sm-6 col-md-6">
                <p><i className="far fa-copyright"></i> COPPYRIGHT 2020 @T - R COMPANY</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );

}



export default Footer;