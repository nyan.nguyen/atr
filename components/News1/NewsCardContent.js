import React from 'react';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import { Typography } from 'antd';

const NewsCardContent = ({props}) => {
  const {products} = props;

  const { t } = useTranslation('common');
  return (
    <>
      <section className="blog-area ptb-100">
        <div className="container">
          <div className="row">
            {products.map(product => {
              return(<div className="col-lg-4 col-md-6">
              <div className="single-blog-post text-center">
                <div className="cover-container">
                  <img className="cover"src={product.cover}/>
                  <div className="cover-middle">
                    <Link href={`/product/${product._id}`}>
                      <a className="default-btn">{t('Products.14')} <span></span></a>
                    </Link>
                  </div>
                </div>
                <div className="post-content">
                  <h3>
                    <Link href={`/product/${product._id}`}>
                      <a>{product?.name?.find(n => n.locale === props._nextI18Next.initialLocale).value}</a>
                    </Link>
                  </h3>
                </div>
              </div>
            </div>
            )
            })}
          </div>
        </div>
      </section>
    </>
  );

}

export default NewsCardContent;