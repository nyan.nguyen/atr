import React, { Component } from 'react'

export class DetailsThumb extends Component {
    render() {
        const {images, tab, myRef} = this.props;
        return (
            <div className="row" style={{justifyContent: "flex-start"}} ref={myRef}>
                {
                images && images.map((img, index) =>(
                    <div className="col-2 mb-3 details-thumb">
                        <img src={img} alt="" key={index} 
                        onClick={() => tab(img)}
                        />
                    </div>
                ))
                }
            </div>
        )
    }
}

export default DetailsThumb