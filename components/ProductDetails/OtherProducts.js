import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
// import { withTranslation } from 'react-i18next';
import { useTranslation } from 'next-i18next';

const OwlCarousel = dynamic(import('react-owl-carousel3'));

const options = {
  loop: true,
  nav: false,
  dots: false,
  autoplayHoverPause: true,
  autoplay: true,
  margin: 30,
  navText: [
    "<i class='flaticon-left-chevron'></i>",
    "<i class='flaticon-right-chevron'></i>"
  ],
  responsive: {
    0: {
      items: 1,
      margin: 10,
    },
    576: {
      items: 1,
    },
    768: {
      items: 2,
    },
    1200: {
      items: 3,
    }
  }
}

const OtherProducts = ({props}) => {
  const {product,products} = props;
  const { t } = useTranslation('common');
  return (
    <>
      <section className="partner-area pb-5 bg-f8f8f8 other-product-area">
        <div className="container">
          <div className="partner-title other-product">
            <h2>{t('ProductDetail.6')}</h2>
          </div>

          {typeof window !== 'undefined' && <OwlCarousel
            className="partner-slides owl-carousel owl-theme"
            {...options}
          >
            {products.map(p => (
              p._id!==product._id &&
                <div className="single-blog-post text-center">
                  <div className="cover-container">
                    <img className="cover"src={p.cover}/>
                    <div className="cover-middle">
                      <Link href={`/product/${pageYOffset._id}`}>
                        <a className="default-btn">{t('Products.14')} <span></span></a>
                      </Link>
                    </div>
                  </div>
                  <div className="post-content">
                    <h3>
                      <Link href={`/product/${p._id}`}>
                        <a>{p?.name?.find(n => n.locale === props._nextI18Next.initialLocale).value}</a>
                      </Link>
                    </h3>
                  </div>
                </div>
              
            ))}
          </OwlCarousel>}
        </div>
      </section>
    </>
  );

}



export default OtherProducts;