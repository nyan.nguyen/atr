import React, { Component, useRef, useState } from 'react';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import ImageGallery from 'react-image-gallery';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

const ProductTab = ({props}) => {
    const {product} = props;
    const [imgIndex, setImgIndex] = useState(0);

    const { t } = useTranslation('common');
    const openTabSection = (evt, tabNmae) => {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabs_item");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByTagName("li");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("current", "");
        }

        document.getElementById(tabNmae).style.display = "block";
        evt.currentTarget.className += "current";
    }

        return (
            <>
                {/* <section className="floor-plans-area ptb-100"> */}
                <div className="product-tab">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12">
                            <div className="card-container">
                                <Tabs type="card">
                                    <TabPane tab={t('ProductDetail.4')} key="1" forceRender>
                                        <div dangerouslySetInnerHTML={{__html: product?.description.find(n => n.locale === props._nextI18Next.initialLocale)?.value}}></div>
                                    </TabPane>
                                    <TabPane tab={t('ProductDetail.5')} key="2">
                                        {
                                            product.certifications.length?
                                                <h5 className="text-center">{product.certifications[imgIndex][props._nextI18Next.initialLocale==="vn"?0:1]?.value}</h5>
                                            :null
                                        }
                                        <ImageGallery onSlide={(currentIndex) => setImgIndex(currentIndex)} showNav={false} showFullscreenButton={false} infinite={false} showPlayButton={false} items={
                                                    product?.certificationsImg?.length?product?.certificationsImg.map((img,idx) => (
                                                        {
                                                            original: img,
                                                            thumbnail: img
                                                        }
                                                    )):[]
                                        }/>
                                    </TabPane>
                                </Tabs>
                            </div>
                                {/* <div className="tab">
                                    <ul className="tabs">
                                        <li
                                            className="current"
                                            onClick={(e) => openTabSection(e, 'tab1')}
                                        >
                                            {t('ProductDetail.4')}
                                            </li>

                                        <li onClick={(e) => openTabSection(e, 'tab2')}>
                                        {t('ProductDetail.5')}
                                            </li>
                                    </ul>

                                    <div className="tab_content">
                                        <div id="tab1" className="event-details-desc tabs_item">
                                            <div dangerouslySetInnerHTML={{__html: product?.description.find(n => n.locale === props._nextI18Next.initialLocale)?.value}}></div>
                                        </div>
                                        <div id="tab2" className="event-details-desc tabs_item">
                                            {
                                                product.certifications.length?
                                                    <h5 className="text-center">{product.certifications[imgIndex][props._nextI18Next.initialLocale==="vn"?0:1]?.value}</h5>
                                                :null
                                            }
                                            <ImageGallery onSlide={(currentIndex) => setImgIndex(currentIndex)} showNav={false} showFullscreenButton={false} infinite={false} showPlayButton={false} items={
                                                        product?.certificationsImg?.length?product?.certificationsImg.map((img,idx) => (
                                                            {
                                                                original: img,
                                                                thumbnail: img
                                                            }
                                                        )):[]
                                            }/>
                                        </div>
                                    </div>
                                </div>
                             */}
                            </div>
                        </div>
                    </div>
                </div>

                {/* </section> */}
            </>
        );
    }

export default ProductTab;