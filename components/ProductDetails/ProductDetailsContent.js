import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import Colors from './Colors';
import DetailsThumb from './DetailsThumb';
import { Image, Select } from 'antd';
import ImageGallery from 'react-image-gallery';

const ProductDetailsContent = ({ props }) => {
    const { product, variations } = props;
    const { t } = useTranslation('common');
    const [index, setIndex] = useState(0);
    const [mainPhoto, setMainPhoto] = useState("");

    const [variantEnable, setVariantEnable] = useState([]);
    const [variantDescription, setVariantDescription] = useState("");

    const myRef = React.createRef();

    const handleTab = img => {
        setMainPhoto(img);
    }

    useEffect(() => {
        if (product) {
            // console.log(product)
            setMainPhoto(product?.cover);
            product?.variants?.map((variation, idx) => {
                return variation.attributeGroups.map((attributeGroup, idx) => {
                    let group = variations.find(v => v._id === attributeGroup.split("#")[0]);
                    if (group.name.toLowerCase() !== "colors") {
                        if (!variantEnable.includes(attributeGroup.split("#")[0])) setVariantEnable([...variantEnable, attributeGroup.split("#")[0]])
                    }
                })
            })
        }
    }, [product])

    useEffect(() => {
        // myRef.current.children[index].className = "active";
    }, []);

    const onDropDownSelect = (value) => {
        // console.log(value)
        product.variants.forEach(variant => {
            if (variant.attributeGroups.includes(value)) {
                let group = variations.find(v => v._id === value.split("#")[0]);
                let variantValue = group ? group.values.find(v => v._id === value.split("#")[1]) : {};
                setVariantDescription(variantValue.description)
                setMainPhoto(product.galery[variant.images[0]]);
            }
        })
    }

    return (
        <>
            <section className="event-details-area pt-5">
                <div className="container">
                    <div className="row">
                        <div className="event-details">
                            <div className="event-details-header">
                                <Link href="/products">
                                    <a className="back-all-event">
                                        <i className="flaticon-left-chevron"></i> {t('ProductDetail.2')}
                                    </a>
                                </Link>
                            </div>

                            <div className="product-detail">
                                <div className="details container d-inline-block" key={product?._id}>
                                    <div className="row w-100 d-inline-block">
                                        <div className="col-12 col-md-6 px-5 d-inline-block">
                                            <ImageGallery 
                                                showNav={false} 
                                                showFullscreenButton={false} 
                                                infinite={false} 
                                                showPlayButton={false} 
                                                autoPlay
                                                items={
                                                    product?.galery?.length?product?.galery.map(img => (
                                                        {
                                                            original: img,
                                                            thumbnail: img,
                                                            originalClass: "product-slider-original",
                                                            thumbnailClass: "product-slider-thumbnail",
                                                        }
                                                    )):[]
                                                }
                                            />
                                        </div>
                                        <div className="col-12 col-md-6 px-5 d-inline-block" style={{verticalAlign: "top"}}>
                                            <div className="row">
                                                <div class="col-12 box m-0">
                                                    <h2 className="p-0">{product?.name.find(n => n.locale === props._nextI18Next.initialLocale).value}</h2>
                                                </div>
                                                <div class="col-12">
                                                        {product?.variants?.map((variation, idx) => {
                                                        return variation.attributeGroups.map((attributeGroup, idx) => {
                                                            let group = variations.find(v => v._id === attributeGroup.split("#")[0]);
                                                            let value = group ? group.values.find(v => v._id === attributeGroup.split("#")[1]) : {};
                                                            if (group.name.toLowerCase() === "colors") {
                                                                return <div className="colors d-inline-block">
                                                                    <button onClick={() => (variation.images.length && product?.galery.length) ? handleTab(product?.galery[variation.images[0]]) : null} style={{ background: value.value }} key={value._id}></button>
                                                                </div>
                                                            }
                                                        })
                                                    })}
                                                </div>
                                                <div class="col-12">
                                                    {variantEnable.map(vId => (
                                                        <Select onChange={onDropDownSelect} style={{ width: "100%" }} defaultValue={t('select.variants.placeholder')}>
                                                            {variations.find(v => v._id === vId).values.map(value => (
                                                                <Select.Option value={`${vId}#${value._id}`}>{value.name}</Select.Option>
                                                            ))}
                                                        </Select>
                                                    ))}
                                                </div>
                                                <div class="col-12">
                                                    <pre>{product?.short_description.find(n => n.locale === props._nextI18Next.initialLocale)?.value}</pre>
                                                </div>
                                                <div class="col-12">
                                                    <pre>{variantDescription}</pre>
                                                </div>
                                            </div>

                                            {/* <div className="colors">
                                                <button style={{background: color}} key={index}></button>
                                            </div> */}

                                            
                                            
                                            {/* <DetailsThumb images={product?.galery} tab={handleTab} myRef={myRef} /> */}
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default ProductDetailsContent;