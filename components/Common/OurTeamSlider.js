import React, { Component } from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
import {useTranslation} from 'next-i18next';
const OwlCarousel = dynamic(import('react-owl-carousel3'));

const options = {
    loop: true,
    nav: true,
    dots: true,
    autoplayHoverPause: true,
    autoplay: true,
    center: true,
    margin: 30,
    navText: [
        "<i class='flaticon-left-chevron'></i>",
        "<i class='flaticon-right-chevron'></i>"
    ],
    responsive: {
        0: {
            items: 1,
        },
        576: {
            items: 2,
        },
        768: {
            items: 2,
        },
        1200: {
            items: 3,
        }
    }
}

const OurTeamSlider = () =>  {
    const {t} = useTranslation('common');
        return (
            <>
                <section className="team-area pb-100">
                    <div className="container">
                        <div className="section-title">
                            <span className="sub-title" style={{fontSize: "40px"}} >{t('About Us.4')}</span>
                            <h2>{t('About Us.5')}</h2>
                        </div>

                        {typeof window !== 'undefined' && <OwlCarousel 
                            className="team-slides owl-carousel owl-theme"
                            {...options}
                        >
                            <div className="single-team-box">
                                <div className="image">
                                    <img src="/images/team/team3.jpg" alt="image" />
                                </div>
                                <div className="content">
                                    <h3>{t('About Us.6')}</h3>
                                    <span>{t('About Us.8')}</span>
                                </div>
                            </div>
                            <div className="single-team-box">
                                <div className="image">
                                    <img src="/images/team/team2.png" alt="image" />
                                </div>

                                <div className="content">
                                    <h3>{t('About Us.7')}</h3>
                                    <span>{t('About Us.9')}</span>
                                </div>
                            </div>

                            
                            <div className="single-team-box">
                                <div className="image">
                                    <img src="/images/team/team4.jpg" alt="image" />
                                </div>
                                <div className="content">
                                <h3>{t('About Us.10')}</h3>
                                    <span></span>
                                </div>
                            </div>
                        </OwlCarousel>}
                    </div>
                </section>
            </>
        );
    }


export default OurTeamSlider;