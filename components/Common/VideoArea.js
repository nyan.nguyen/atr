import React, { useState, useRef } from 'react';
import ModalVideo from 'react-modal-video';
import { useTranslation } from 'next-i18next';
import VisibilitySensor from 'react-visibility-sensor';
import { Player } from 'video-react';

const VideoArea = ({ gif }) => {
  const [isOpen, setIsOpen] = useState(false)
  const { t } = useTranslation('common')
  const videoRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);

  React.useEffect(() => {
    if (isVisible) {
      // videoRef.current.play();
    } else {
      if (videoRef.current.play) {
        videoRef.current.pause();
      }
    }
  }, [isVisible]);
  const handlePlayVideo = () => {
    if (videoRef.current.play) {
      vidRef.current.pause();
    } else {
      vidRef.current.play();
    }
  }
  return (
    <>
      <section className="video-area ptb-100 pb-0">
        <div className="container">
          <div className="video-content">
            <h2>{t('Watch Video.1')}</h2>
          </div>
          
          {/* <div style={{display:"flex",justifyContent:"center"}}>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/bERFXDESDdE?controls=0" title="YouTube video player" frameBorder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
          </div> */}

          <div className="video">
            <VisibilitySensor onChange={(isVisible) => setIsVisible(isVisible)}>
              <video  controls poster="/images/video-img.png" ref={videoRef}>
                <source src='/images/gallery/videoatr.mp4' type='video/mp4' />
              </video>
            </VisibilitySensor>
          </div>

          
        </div>
      </section>



      {/* <ModalVideo  autoplay
        channel='custom'
        url='/images/gallery/videoatr.mp4'
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
      /> 
          <div className="video">
            <VisibilitySensor onChange={(isVisible) => setIsVisible(isVisible)}>
              <video  loop ref={videoRef}>
                <source src='/images/gallery/videoatr.mp4' type='video/mp4' />
              </video>
            </VisibilitySensor>
          </div>*/}

    </>
  );
}



export default VideoArea;