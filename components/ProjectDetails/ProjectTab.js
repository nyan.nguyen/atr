import React, { Component } from 'react';
import { useTranslation } from 'next-i18next';

const ProjectTab = ({props}) => {
    const {project} = props;

    const { t } = useTranslation('common');
    const openTabSection = (evt, tabNmae) => {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabs_item");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByTagName("li");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("current", "");
        }

        document.getElementById(tabNmae).style.display = "block";
        evt.currentTarget.className += "current";
    }

        return (
            <>
                {/* <section className="floor-plans-area ptb-100"> */}
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12">
                            <div className="tab">
                                <ul className="tabs">
                                    <li
                                        className="current"
                                        onClick={(e) => openTabSection(e, 'tab1')}
                                    >
                                       {t('ProductDetail.4')}
                                    </li>
                                </ul>

                                <div className="tab_content">
                                    <div id="tab1" className="event-details-desc tabs_item">
                                        <pre>
                                        {project?.description.find(n => n.locale === props._nextI18Next.initialLocale)?.value}
                                        </pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* </section> */}
            </>
        );
    }

export default ProjectTab;