import React, { Component, useState, useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { Image } from 'antd';
import dynamic from 'next/dynamic';

const OwlCarousel = dynamic(import('react-owl-carousel3'));

const ProjectDetailsContent = ({ props }) => {
    const { project } = props;
    const [mainPhoto, setMainPhoto] = useState("");

    useEffect(() => {
        if (project) {
            setMainPhoto(project?.cover);
        }
    }, [project])

    return (
        <>
            <section className="event-details-area ptb-100">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center"><h2 style={{textDecoration: "none"}}>{project?.name.find(n => n.locale === props._nextI18Next.initialLocale).value}</h2></div>
                        <div className="col-12"><Image width="100%" src={mainPhoto} /></div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default ProjectDetailsContent;