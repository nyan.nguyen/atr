import React from 'react';
import Link from 'next/link';
// import { withTranslation } from 'react-i18next';
import {useTranslation} from 'next-i18next';
import { Typography } from 'antd';

const Projects = ({props}) => {
  const {projects} = props;
  const {t} = useTranslation('common');
  return (
    <>
      <section className="blog-area ptb-100">
        <div className="container">
          <div className="row">
            {projects.map(project => {
              return(<div className="col-lg-4 col-md-6">
              <div className="single-blog-post text-center">
                <div class="cover-container">
                  <img className="cover"src={project.cover}/>
                  <div class="cover-middle">
                    <Link href={`/project/${project._id}`}>
                      <a className="default-btn">{t('Products.14')} <span></span></a>
                    </Link>
                  </div>
                </div>

                <div className="post-content">
                  <h3>
                    <Link href={`/project/${project._id}`}>
                      <a>{project?.name?.find(n => n.locale === props._nextI18Next.initialLocale).value}</a>
                    </Link>
                  </h3>
                  <div className="row">
                    <Typography.Text className="short-description-preview">{project?.description?.find(n => n.locale === props._nextI18Next.initialLocale)?.value}</Typography.Text>
                  </div>
                </div>
              </div>
            </div>
            )
            })}
          </div>
        </div>
      </section>
    </>
  );

}


export default Projects;