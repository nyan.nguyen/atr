import React from 'react';
import Link from 'next/link';
// import { withTranslation } from 'react-i18next';
import {useTranslation} from 'next-i18next';
import {serverSideTranslations} from 'next-i18next/serverSideTranslations';
import ImageGallery from 'react-image-gallery';

const AboutUs = () => {
  const {t} = useTranslation('common');
  return (
    <>
      <section className="about-section ptb-100">
        <div className="container-fluid">
          <div className="row align-items-center">
            <div className="col-lg-7 col-md-12">
              <div style={{margin:"3%"}}>
                {/* <div className="img1">
                  <img width="500" src="/images/company/combined_image.png" alt="image"/>
                </div> */}
                <div className="img1">
                  <ImageGallery 
                    showNav={false} 
                    showFullscreenButton={false} 
                    infinite={true} 
                    autoPlay={true}
                    showPlayButton={false} 
                    showThumbnails={false}
                    items={[
                      {
                        original: "/images/company/DSCF6464.jpg",
                        thumbnail: "/images/company/DSCF6464.jpg"
                      },
                      {
                        original: "/images/company/DSCF6385.jpg",
                        thumbnail: "/images/company/DSCF6385.jpg"
                      },
                      {
                        original: "/images/company/DSCF6374.jpg",
                        thumbnail: "/images/company/DSCF6374.jpg"
                      },
                    ]}
                  />
                </div>
                {/* <div className="img2">
                  <img width="500" src="/images/company/DSCF6464.jpg" alt="image"/>
                </div>

                <div className="img3">
                  <img width="500" src="/images/company/DSCF6385.jpg" alt="image"/>
                </div> */}
              </div>
            </div>

            <div className="col-lg-5 col-md-12">
              <div className="about-content">
                <span className="sub-title">{t('About Us.1')}</span>
                <h2>{t('Subtitle About Us 1.1')}</h2>
                <p>{t('Subtitle About Us 2.1')}</p>
                <Link href="/about">
                  <a className="read-more-btn">
                    {t('About Us.11')} <i className="flaticon-next"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );

}



export default AboutUs;