import React, { Component } from 'react';
import NavbarTwo from '../components/Layout/NavbarTwo';
import PageHeader from '../components/Services/PageHeader';
import NewsCardContent from '../components/News1/NewsCardContent';
import Footer from '../components/Layout/Footer';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import { ProductsApi } from '../api';

class Products extends Component {
    render() {
        return (
            <>
                <NavbarTwo useSuspense={false} />
                <PageHeader useSuspense={false} />
                <NewsCardContent props={this.props} useSuspense={false} />
                <Footer useSuspense={false}/>
            </>
        );
    }
}

export async function getStaticProps(context) {
  const products = await ProductsApi.getAll();
  
  return {props: {
      products: products?products:[],
      ...await serverSideTranslations(context.locale, ['common']),
  },revalidate: 60}
}

export default Products;