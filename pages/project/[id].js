import React, { Component } from 'react';
import PageHeader from '../../components/ProjectDetails/PageHeader';
import ProjectDetailsContent from '../../components/ProjectDetails/ProjectDetailsContent';
import ProjectTab from '../../components/ProjectDetails/ProjectTab';
import OtherProducts from '../../components/ProjectDetails/OtherProducts';
import { InvestmentsApi } from '../../api';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import NavbarTwo from '../../components/Layout/NavbarTwo';
import Footer from '../../components/Layout/Footer';

const ProjectDetails = (props) => {
    return (
      <>
        <NavbarTwo useSuspense={false} />
        <PageHeader useSuspense={false} />
        <ProjectDetailsContent props={props} useSuspense={false} />
        <ProjectTab props={props} useSuspense={false} />
        <Footer useSuspense={false} />
      </>
    );
}

export default ProjectDetails;


export async function getStaticProps(context) {
    const project = await InvestmentsApi.get({
        _id: context.params.id
    });

    return {props: {
      project: project?project:{},
      ...await serverSideTranslations(context.locale, ['common']),
    },revalidate: 60}
}

export async function getStaticPaths() {
  const products = await InvestmentsApi.getAll();

  const paths_1 = products.map((product) => ({params: { id: product._id.toString() }, locale: 'vn' }))
  const paths_2 = products.map((product) => ({params: { id: product._id.toString() }, locale: 'en' }))

  const paths = [...paths_1, ...paths_2]

  // console.log(paths)

  // fallback: false means pages that don’t have the
  // correct id will 404.
  return { 
    paths, 
    fallback: false 
  }
}
