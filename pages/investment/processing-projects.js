import React, { Component } from 'react';
import NavbarTwo from '../../components/Layout/NavbarTwo';
// import Navbar from '../../components/Layout/Navbar';
import PageHeader from '../../components/ProcessingProjects/PageHeader';
import Footer from '../../components/Layout/Footer';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import Projects from '../../components/CompletedProjects/Projects';
import { InvestmentsApi } from '../../api';

class About extends Component {
    render() {
        return (
            <>
                <NavbarTwo useSuspense={false} />
                <PageHeader useSuspense={false} />
                <Projects props={this.props} useSuspense={false}/>
                <Footer useSuspense={false} />
            </>
        );
    }
}

export async function getStaticProps(context) {
    const projects = await InvestmentsApi.getProcessingProjects();
    return {props: {
      projects: projects?projects:[],
        ...await serverSideTranslations(context.locale, ['common']),
    },revalidate: 60}
}

export default About;