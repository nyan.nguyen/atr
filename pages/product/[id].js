import React, { Component } from 'react';
import PageHeader from '../../components/ProductDetails/PageHeader';
import ProductDetailsContent from '../../components/ProductDetails/ProductDetailsContent';
import ProductTab from '../../components/ProductDetails/ProductTab';
import OtherProducts from '../../components/ProductDetails/OtherProducts';
import { ProductsApi, VariationsApi } from '../../api';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import NavbarTwo from '../../components/Layout/NavbarTwo';
import Footer from '../../components/Layout/Footer';

const ProductDetails = (props) => {
    return (
      <>
        <NavbarTwo useSuspense={false} />
        <PageHeader useSuspense={false} />
        <ProductDetailsContent props={props} useSuspense={false} />
        <ProductTab props={props} useSuspense={false} />
        <OtherProducts props={props} useSuspense={false} />
        <Footer useSuspense={false} />
      </>
    );
}

export default ProductDetails;


export async function getStaticProps(context) {
    const product = await ProductsApi.get({
        _id: context.params.id
    });

    const products = await ProductsApi.getAll();
    const variations = await VariationsApi.getAll();

    return {props: {
        product: product?product:{},
        products: products?products:[],
        variations: variations?variations:[],
        ...await serverSideTranslations(context.locale, ['common']),
    },revalidate: 60}
}

export async function getStaticPaths() {
  const products = await ProductsApi.getAll();

  const paths_1 = products.map((product) => ({params: { id: product._id.toString() }, locale: 'vn' }))
  const paths_2 = products.map((product) => ({params: { id: product._id.toString() }, locale: 'en' }))

  const paths = [...paths_1, ...paths_2]

  // console.log(paths)

  // fallback: false means pages that don’t have the
  // correct id will 404.
  return { 
    paths, 
    fallback: false 
  }
}
